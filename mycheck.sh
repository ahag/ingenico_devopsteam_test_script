#!/usr/bin/env bash
## check for tcp port ##

# check if shyaml
whereis shyaml &> /dev/null

if [ $? -eq 0 ]; then
    for i in $(cat myinput.yaml | shyaml keys checks.ping);do
    url=$(cat myinput.yaml | shyaml get-value checks.ping.$i | tr ':' '/')
    (echo >/dev/tcp/$url) &>/dev/null && printf "\u2714\ $i\n" || printf "\u274c\ $i\n";done
else
    echo "shyaml  is NOT installed! to install do : pip install shyaml"
fi

